import colors from 'tailwindcss/colors'
import plugin from 'tailwindcss/plugin'
import defaultTheme from 'tailwindcss/defaultTheme'

/** @type {import('tailwindcss').Config} */
export default {
    content: ['./src/**/*.{html,js,svelte,ts}'],
    theme: {
        screens: {
            'xsm'   : '360px',
            // 'sm'    : '640px',
            // 'md'    : '768px',
            // 'lg'    : '1024px',
            // 'xl'    : '1280px',
            // '2xl'   : '1536px',
            ...defaultTheme.screens,
        },

        extend: {
            fontFamily: {
                title: ['Pacifico'],
                text: ['Inter'],
            },

            colors: {
                // Site background
                'background'                    : colors.zinc[100],
                'background-dark'               : colors.zinc[800],

                // Card background + shadow
                'card'                          : colors.zinc[50],
                'card-dark'                     : colors.zinc[700],
                'card-shadow'                   : colors.zinc[950],

                // Dropdown + shadow
                'dropdown'                      : colors.zinc[200],
                'dropdown-dark'                 : colors.zinc[600],
                'dropdown-shadow'               : colors.zinc[950],
                'dropdown-item-selected'        : colors.zinc[100],
                'dropdown-item-selected-dark'   : colors.zinc[500],

                // Primary + Accent
                'primary'                       : colors.indigo[600],
                'primary+1'                     : colors.indigo[800],
                'primary+2'                     : colors.indigo[950],
                'primary-1'                     : colors.indigo[400],
                'primary-2'                     : colors.indigo[300],

                'primary-hover'                 : colors.indigo[100],
                'primary-click'                 : colors.indigo[200],
                'primary-hover-dark'            : colors.zinc[600],
                'primary-click-dark'            : colors.zinc[500],

                'accent'                        : colors.amber[600],
                'accent+1'                      : colors.amber[800],
                'accent-1'                      : colors.amber[400],
                'accent-2'                      : colors.amber[300],

                // For borders (toolbar, cards, ...)
                'border'                        : colors.zinc[950],
                'border-dark'                   : colors.zinc[800],

                // For the title shadow
                'title-shadow'                  : colors.indigo[800],
                'title-shadow-dark'             : colors.indigo[400],

                // Grey text
                'text-2nd'                      : colors.zinc[600],
                'text-2nd-dark'                 : colors.zinc[300],
            },

            textShadow: {
                DEFAULT: '0 0 6px var(--tw-shadow-color)',
            },
        },
    },
    darkMode: 'class',
    plugins: [
        // Text shadow (for title)
        plugin(function ({ matchUtilities, theme }) {
            matchUtilities(
                {
                    'text-shadow': (value) => ({
                        textShadow: value,
                    }),
                },
                { values: theme('textShadow') }
            )
        }),
    ],
}

