# Translator Frontend

# Development
## Prerequisites
-   Node and npm

## Getting started
-   Clone this repository and `cd` into it
-   `npm install`
-   `npm run dev`
-   Go to `http://localhost:5173`

## Useful commands
```bash
# Start development server
npm run dev

# Build frontend as SPA (single page application; static)
npm run build

# Preview the built SPA
npm run preview

# Initial deploy
fly launch

# Subsequent deployments
fly deploy

```
