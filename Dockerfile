# Stage 1 - Build Frontend #####################################################
FROM node:18-alpine as frontend_build

WORKDIR /app
COPY ./package*.json .
RUN npm ci
COPY . .
RUN npm run build
RUN npm prune --omit=dev

# Stage 2 - Build container image ##############################################
FROM node:18-alpine as frontend_deploy

WORKDIR /app
RUN rm -rf ./*
COPY --from=frontend_build /app .

EXPOSE 3000
CMD ["node", "./build"]

################################################################################
