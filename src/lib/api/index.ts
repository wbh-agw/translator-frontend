// Imports /////////////////////////////////////////////////////////////////////

// Constants ///////////////////////////////////////////////////////////////////
const baseUrl = (import.meta.env.VITE_SERVER_URL || 'http://localhost') + '/api'

// Translate Function //////////////////////////////////////////////////////////

/**
 * Translate text.
 */
export async function translate(text: string) {
    const res = await fetch(`${baseUrl}/translate`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ text }),
    })

    const data = await res.json()

    return data as { text: string, translation: string }
}

/**
 * Save translation to file.
 */
export async function save(text: string, translation: string) {
    const res = await fetch(`${baseUrl}/download`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Accept': '*/*' },
        body: JSON.stringify({ text, translation }),
    })

    const data = await res.blob()
    return data
}

////////////////////////////////////////////////////////////////////////////////
