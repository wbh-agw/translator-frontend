import { writable } from "svelte/store"


export type LanguageCode = 'de' | 'en-us' | 'it' | 'fr'
export type LanguageFlag = 'De' | 'Us' | 'It' | 'Fr'

export interface Language {
    name: string,
    code: LanguageCode,
    flag: LanguageFlag,
}

class Languages {
    public readonly list: Language[] = [
        { name: 'German'       , code: 'de'    , flag: 'De' },
        { name: 'English (US)' , code: 'en-us' , flag: 'Us' },
        { name: 'Italian'      , code: 'it'    , flag: 'It' },
        { name: 'French'       , code: 'fr'    , flag: 'Fr' },
    ]
        .sort((a, b) => {
            if (a.name > b.name) { return  1 }
            if (a.name < b.name) { return -1 }
            return 0

        }) as Language[]

    public find(code: LanguageCode): Language {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        return this.list.find(l => l.code === code)!
    }
}

export const languages = new Languages()


export const srcLangCode = writable('en-us')
export const dstLangCode = writable('it')

export const srcText = writable('')
export const dstText = writable('')

export const isTranslated = writable(false)