import { get, writable } from 'svelte/store';

const { subscribe, update, set: _set } = writable(false)

function set(b: boolean) {
    // Set store value
    _set(b)

    // Get <html id="root"> element
    const root = document.getElementById('app-root')

    // Set the class on the root <html> element
    if (root) {
        root.classList.remove('dark')
        if (get(darkMode) === true) { root.classList.add('dark') }
    }
}

function toggle() {
    // Update the store value
    update(current => !current)

    // Get <html id="root"> element
    const root = document.getElementById('app-root')

    // Set the class on the root <html> element
    if (root) {
        root.classList.remove('dark')
        if (get(darkMode) === true) { root.classList.add('dark') }
    }
}

export const darkMode = {
    subscribe,
    set,
    toggle
}